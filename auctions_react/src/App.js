import React from "react";
import Login from "./Views/Authentication/Login/Login";
import Alert from "./Views/Alerts/alertContainer";
function App() {
  return (
    <>
      <Alert />
      <Login />
    </>
  );
}

export default App;
