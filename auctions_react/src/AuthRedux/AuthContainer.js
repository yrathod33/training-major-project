import { connect } from 'react-redux';

import privateRouter from '../Routers/privateRouter';

const mapStateToProps = state => {
  return {
    auth:state.isAuth
  };
};
export default connect(
  mapStateToProps
)(privateRouter);