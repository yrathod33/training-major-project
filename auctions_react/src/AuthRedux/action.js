export const login = isAuth => {
  return {
    type: "AUTH_INIT",
    auth: isAuth
  };
};
export const logout = isAuth => {
  return {
    type: "AUTH_DESTROY",
    auth: isAuth
  };
};
export function authInit(dispatch, isAuth) { 
  dispatch(login(isAuth));
}


