const reducer = (
    state = {
      auth:false,
    },
    action
  ) => {
    switch (action.type) {
      case "AUTH_INIT":
        return {
          auth: action.isAuth,
        };
      case "AUTH_DESTROY":
        return {
          auth: action.isAuth,
        };
      default:
        return state;
    }
  };
  export default reducer;