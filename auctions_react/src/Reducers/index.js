import { combineReducers } from 'redux';
import auth from '../AuthRedux/reducer';
import alert from '../Views/Alerts/alertReducer';

export default combineReducers({
  alert
})