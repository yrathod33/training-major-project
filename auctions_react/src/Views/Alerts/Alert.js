import React, { Component } from 'react'
import './alert-design.css'

export default class Alert extends Component {   
    render() {        
        const {msg,cssClass} = this.props;
        return (
            <div className={cssClass} >
                <h1 >{msg}</h1>
            </div>
        )
    }
}