import { connect } from "react-redux";
import Alert from "./Alert";
const mapStateToProps = state => {
  return {
    msg:state.message,
    cssClass: state.cssClass
  };
};
export default connect(
  mapStateToProps
)(Alert);
