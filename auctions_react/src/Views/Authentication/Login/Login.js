import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import LoginUI from "./Login_UI";
import "../../Alerts/alert-design.css";
import { connect } from "react-redux";
import { showAlertWithTimeout } from "../../Alerts/actions";
// import { authInit } from "./../../../AuthRedux/action";
import RegForm from "../../Registration/RegForm";
import Profile from "../../../Views/Profile/userProfile";
import EditProfile from "../../../Views/Profile/edit_profile";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLogin: false,
      password: "",
      email: "",
      wrongPassword: false
    };        
  }

  waitForAction(callback, secondCallback) {
    callback();
    secondCallback();
  }
  afterSubmission = async e => {
    e.preventDefault();
    const response = await fetch("/login-user", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        email: this.state.email,
        password: this.state.password
      })
    });
    const body = await response.json();

    if (body.code === 200) {
      this.waitForAction(
              () =>
                showAlertWithTimeout(
                  this.props.dispatch,
                  body.success,
                  "success"
                ),
              () =>
                this.setState({ isLogin: true }, () => {

                  localStorage.setItem("token", body.token);
                })
            );
      
    } 
    else
    {
      this.setState({ wrongPassword: true }, () => {
              showAlertWithTimeout(
                this.props.dispatch,
                body.failed,
                "error"
              );
            });
    }
  };

  // checkLogin = () => {
  //   const username = this.state.username;
  //   const password = this.state.password;
  //   if (username === "admin" && password === "admin") {
  //     this.waitForAction(
  //       () =>
  //         showAlertWithTimeout(
  //           this.props.dispatch,
  //           "Login Successful",
  //           "success"
  //         ),
  //       () =>
  //         this.setState({ isLogin: true }, () => {
  //           localStorage.setItem("user", "true");
  //         })
  //     );
  //   } else {
  //     this.setState({ wrongPassword: true }, () => {
  //       showAlertWithTimeout(
  //         this.props.dispatch,
  //         "Wrong username or password",
  //         "error"
  //       );
  //     });
  //   }
  //   authInit(this.props.dispatch, false);
  // };

  handleInput = event => {
    if (event.target.id === "email")
      this.setState({ email: event.target.value });
    else this.setState({ password: event.target.value });
  };

  signOut = history => {
    this.waitForAction(
      () =>
        showAlertWithTimeout(
          this.props.dispatch,
          "Logout Successfully",
          "success"
        ),
      () => {
        this.setState({ isLogin: false }, () => {
          localStorage.removeItem("user");
          history.push("/");
        });
      }
    );
  };

  showAlert = (bannerDisplay, none) => {
    bannerDisplay();
    setTimeout(none, 1500);
  };

  render() {
    return (
      <div>
        <Router>
          <Switch>
            <Route
              path="/login"
              exact
              render={props => (
                <LoginUI
                  {...props}
                  {...this.props}
                  isLogin={this.state.isLogin}
                  afterSubmission={this.afterSubmission}
                  handleInput={this.handleInput}
                  wrongPassword={this.state.wrongPassword}
                  showAlert={this.showAlert}
                />
              )}
            />
            <Route path="/register" component={RegForm} />

            <Route path="/home" render={props => <h1>Homepage</h1>} />

            <Route path="/pr" component={Profile} />
            <Route path="/edit-user" component={EditProfile} />
          </Switch>
        </Router>
      </div>
    );
  }
}
export default connect()(Login);
