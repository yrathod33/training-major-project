import "./design.css";
import React, { Component } from "react";
import { Redirect, Link } from "react-router-dom";
import { connect } from "react-redux";
class LoginUI extends Component {
  render() {
    let { afterSubmission, handleInput, isLogin, wrongPassword } = this.props;    
    if (isLogin || localStorage.getItem("token")) {
      return <Redirect to="/home" />
    }
    return (
      <form onSubmit={afterSubmission}>
        <div className="container-box">
          <div className="login-box">
            <div className="header">
              <h2>LOGIN</h2>
            </div>
            <div className="box-body">
              <input
                className="login-input"
                id="email"
                onChange={handleInput}
                type="text"
                placeholder="Username"
              />
              <input
                className="login-input"
                id="password"
                onChange={handleInput}
                type="password"
                placeholder="Password"
              />
            </div>
            <div className="footer">
              <input
                className="login-button"
                type="submit"
                value="LogIn"
              />
            </div>
            <div>
              <p className={wrongPassword ? "show-warning" : "hide-warning"}>
                Please enter correct username or password
              </p>
            </div>
            <div>
              <Link to="/register">"Don't have account Register here.."</Link>
            </div>
          </div>
        </div>
      </form>
    );
  }
}
export default connect()(LoginUI);
