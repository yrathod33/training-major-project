import React from "react";
import "./box-design.scss";
export default function AdBox(props) {
  return (
    <div className="outer-box">
      <div className="item-title-box">
        <p>
          This is the title of the that item. this can be much bigger in realty
        </p>
      </div>
      <div className="item-image">
        <img
          src="http://emporiumantiques.com/wp-content/uploads/2018/04/girl-w-skirt.jpg"
          alt="pics"
        />
      </div>
      <div className="current-bid">
            <p>Current Bid</p>
            <p className="bid-price">$ 9999.99890088</p>
      </div>
      <div className="valid-date">
            <p>Ends on 25 july 12 UTS</p>
      </div>
    </div>
  );
}
