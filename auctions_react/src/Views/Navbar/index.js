import React from "react";
import "./navbar.scss";
export default function Navbar(props) {
  return (
    <div className="navbar-root">
      <div className="logo">
        <img
          src="https://i.pinimg.com/originals/9f/e5/34/9fe5345bd3f495eda87f2c5e535e40b8.png"
          width="60px"
          alt="logo"
        />
      </div>
      <div className="search-container">
      <div className="search">
          <input className="search-input" type="text" placeholder="Search" />
          <button class="search-button"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/8e/Feedbin-Icon-home-search.svg/768px-Feedbin-Icon-home-search.svg.png" width="25px" alt="search"></img></button>
      </div>
      </div>
      <div className="profile">
        <img
          src="https://images.unsplash.com/photo-1518806118471-f28b20a1d79d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"
          width="40px"
          height="40px"
          alt="profile"
        />
        <div class="dropdown-content">
          <p>Profile</p>
          <p>Sign Out</p>
        </div>
      </div>
    </div>
  );
}
