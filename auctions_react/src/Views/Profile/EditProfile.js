import React, { useState, useEffect } from "react";
import "./profile_design.scss";
export default function UserProfile(props) {
  const [name, setName] = useState("");
  const [bio, setBio] = useState("");

  useEffect(() => {
    setName("Yogesh Rathod");
    setBio("this is bio");
  }, []);

  const handler = event => {
    if (event.target.id === "name") setName(event.target.value);
    else setBio(event.target.value);
  };

  return (
    <form>
      <div className="container">
        <div className="box">
        <div className="header">
          <h2>Profile</h2>
        </div>
        <div className="profile_pic_container">
          <img
            className="profile_pic"
            src="https://images.unsplash.com/photo-1518806118471-f28b20a1d79d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"
            alt="pic"
          />
        </div>
        <div className="username">
          {/* <p>Yogesh Rathod</p> */}
          <input
            className="name"
            type="text"
            id="name"
            value={name}
            onChange={handler}
            placeholder="First Last"
          />
          <br />
          <p className="email">Yogesh@rathod.com</p>
        </div>
        <div className="bio">
          {/* <p>Mohandas Karamchand Gandhi was born on 2 October 1869 into a Gujarati Hindu Modh Baniya family in Porbandar (also known as Sudamapuri), a coastal town on the Kathiawar Peninsula and then part of the small princely state of Porbandar in the Kathiawar Agency of the Indian Empire.</p> */}
          <textarea
            type="text"
            maxLength="150"
            id="bio"
            value={bio}
            onChange={handler}
            placeholder="Your bio here.."
          />
          
        </div>
            <div className="button-pos">
                <button className="edit-profile-button">Save Profile</button>
            </div>
        </div>
        
      </div>
      
    </form>
  );
}
