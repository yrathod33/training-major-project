import React, { useState } from "react";
import './profile_design.scss';
import { Redirect } from "react-router-dom";


function UserProfile(props) 
{
    const [edit, setEdit] = useState(false);
    if (edit) {
        return <Redirect to='/edit-user' />;
    }
    return (
    <div>
        <div className="container">
        <div className="header">
            <h2>Profile</h2>
        </div>
        <div className='profile_pic_container'>
        <img className="profile_pic" src="https://images.unsplash.com/photo-1518806118471-f28b20a1d79d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80" alt="pic"/>
        </div>
        <div className="username">
            <p>Yogesh Rathod</p>
            <p className="email">Yogesh@rathod.com</p>
        </div>
        <div className="bio">
            <p>Mohandas Karamchand Gandhi was born on 2 October 1869 into a Gujarati Hindu Modh Baniya family in Porbandar (also known as Sudamapuri), a coastal town on the Kathiawar Peninsula and then part of the small princely state of Porbandar in the Kathiawar Agency of the Indian Empire.</p>
        </div>
        
    </div>
   - <button className="edit-profile-button" onClick={()=>{setEdit(true)}}>Edit Profile</button>
    
    </div>
);  
}
export default UserProfile;