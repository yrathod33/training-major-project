import React, { useState } from "react";
import "../Registration/design.css";
import { Redirect, Link } from "react-router-dom";
import { connect } from "react-redux";
import { showAlertWithTimeout } from "../Alerts/actions";
function Register(props) {
  const [name, setName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isReg, setIsReg] = useState(false);

  const handleInput = event => {
    if (event.target.id === "fname") setName(event.target.value);
    if (event.target.id === "lname") setLastName(event.target.value);
    if (event.target.id === "email") setEmail(event.target.value);
    if (event.target.id === "password") setPassword(event.target.value);
  };

  const afterSubmission = async e => {
    e.preventDefault();
    const response = await fetch("/register-user", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        fname: name,
        lname: lastName,
        email: email,
        password: password
      })
    });
    const body = await response.json();
    if (body.code === 200) {
      showAlertWithTimeout(props.dispatch, body.success, "success");
      setIsReg(true);
    } else showAlertWithTimeout(props.dispatch, body.failed, "error");
  };
  if (isReg) return <Redirect to="/home" />;
  return (
    <form onSubmit={afterSubmission}>
      <div className="container-box">
        <div className="login-box">
          <div className="box-body">
            <input
              className="login-input"
              id="fname"
              onChange={handleInput}
              type="text"
              value={name}
              placeholder="First Name"
            />
            <input
              className="login-input"
              id="lname"
              onChange={handleInput}
              type="text"
              value={lastName}
              placeholder="Last Name"
            />
            <input
              className="login-input"
              id="email"
              onChange={handleInput}
              type="text"
              value={email}
              placeholder="Email"
            />
            <input
              className="login-input"
              id="password"
              onChange={handleInput}
              type="password"
              value={password}
              placeholder="Password"
            />
          </div>
          <div className="footer">
            <input className="login-button" type="submit" value="SignUp" />
          </div>
          <div>
            <Link to="/login">Already user?</Link>
          </div>
        </div>
      </div>
    </form>
  );
}
export default connect()(Register);
